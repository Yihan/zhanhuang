const indexBanner = new Swiper('.banner-container');
// const mobilerOepnAccount = new Swiper('.sidebar-container', {
//   grabCursor: true,
//   freeMode: true,
//   slidesPerView: 'auto',
// });
window.localeChange = (urlPrefix) => {
  let targetUrl = (function(){
    switch (document.location.pathname) {
      case '/':
      case '/tw':
      case '/tw/':
      case '/ja':
      case '/ja/':
      case '/zh':
      case '/zh/':
        return urlPrefix == '' ? '/' : urlPrefix;
      default:
        let currentPath = document.location.pathname.split('/').filter(item => !!item);
        return (currentPath[0] == 'tw' || currentPath[0] == 'zh' || currentPath[0] == 'ja') ?
          // 現在非英文語系
          (function(){
            let a = currentPath.splice(1);
            let e = '';
            a.forEach(item => e += `/${item}`);
            return `${urlPrefix}${e}`;
          })()
          :
          // 現在為英文 直接跳轉
          `${urlPrefix}${document.location.pathname}`
    }
  })();
  document.location = targetUrl;
}

$(function() {
  var header = $(".header_wrapper");
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 10) {
        header.addClass("change_height");
    } else {
        header.removeClass("change_height");
    }
  });

  $(".header_nav ul li").click(function(){
    $(this).toggleClass("open");
  });

  $(".mob_menu").click(function(){  
    $(".header_nav").toggleClass('open');
  });
});

(function() {
  let currentPath = document.location.pathname.split('/').filter(item => !!item);
  switch (currentPath[0]) {
    case 'ja':
      document.getElementById('initLangImg').src = "/images/common/flag_ja.png";
      document.getElementById('initLangText').textContent = "日本語";
      break;
    case 'zh':
      document.getElementById('initLangImg').src = "/images/common/flag_zh.png";
      document.getElementById('initLangText').textContent = "中文(简体)";
      break;
    case 'tw':
      document.getElementById('initLangImg').src = "/images/common/flag_tw.png";
      document.getElementById('initLangText').textContent = "中文(繁體)";
      break;
    default:
      document.getElementById('initLangImg').src = "/images/common/flag_tw.png";
      document.getElementById('initLangText').textContent = "中文(繁體)";
      break;
  };
  if(/market-reports/.test(document.location.pathname)) {
    $.ajax({
      method: 'GET',
      url: '/reuterslink.json',
    })
    .then(res=>{
      let targetArray = res.entries;
      if(!Array.isArray(targetArray)) return;
      let maxLength = targetArray.length;
      let resultString = "<ul>";
      for(var i=0; i < maxLength; i++) {
        let date = targetArray[i].pubDate.slice(5,22).replace('GMT', '')
        resultString += `<li><a href="${targetArray[i].link}" target="_blank">${date} / ${targetArray[i].title}</a></li>`;
      }
      resultString += "</ul>";
      $('#marketReports').html(resultString);
    });
  }
})();

$(".close").click(function(){
  $('.popup').hide()
})
