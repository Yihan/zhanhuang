const fs = require('fs');

const linkGenerator = (prefix='') => {
  return {
    "/": { url: prefix === '' ? "/" : prefix, template: 'index' },
    "/a/why-zhan-huang/": { url: `${prefix}/a/why-zhan-huang/`, template: 'aboutus_whyzhanhuang' },
    "/a/company-news/": { url: `${prefix}/a/company-news/`, template: 'aboutus_companynews' },
    "/a/our-advantage/": { url: `${prefix}/a/our-advantage/`, template: 'aboutus_ouradvantage' },
    "/2018/06/07/news12/": { url: `${prefix}/2018/06/07/news12/`, template: '20180607_news12' },
    "/2018/06/07/news/": { url: `${prefix}/2018/06/07/news/`, template: '20180607_news' },
    "/a/terms-and-conditions/": { url: `${prefix}/a/terms-and-conditions/`, template: 'terms_and_conditions' },
    "/a/privacy-policy/": { url: `${prefix}/a/privacy-policy/`, template: 'privacy_policy' },
    "/openaccount/": { url: `${prefix}/openaccount/`, template: 'openaccount' },
    "/openaccount/zhan-huang-no-1/": { url: `${prefix}/openaccount/zhan-huang-no-1/`, template: 'zhanhuang_no1' },
    "/deposit/": { url: `${prefix}/deposit/`, template: 'deposit' },
    "/openaccount/withdrawal/": { url: `${prefix}/openaccount/withdrawal/`, template: 'withdrawal' },
    "/knowledge-base/": { url: `${prefix}/knowledge-base/`, template: 'knowledge_base' },
    "/knowledge-base/how-can-i-insert-my-custom-indicator-into-the-trading-platform/": { url: `${prefix}/knowledge-base/how-can-i-insert-my-custom-indicator-into-the-trading-platform/`, template: 'knowledgebase1_1' },
    "/knowledge-base/how-to-insert-chart-indicators/": { url: `${prefix}/knowledge-base/how-to-insert-chart-indicators/`, template: 'knowledgebase1_2' },
    "/knowledge-base/trading-currency/": { url: `${prefix}/knowledge-base/trading-currency/`, template: 'knowledgebase2_1' },
    "/knowledge-base/%e6%9c%80%e5%b0%8f%e4%ba%a4%e6%98%93%e6%89%8b%e6%95%b0%e6%98%af%e5%a4%9a%e5%b0%91%ef%bc%9f/": { url: `${prefix}/knowledge-base/%e6%9c%80%e5%b0%8f%e4%ba%a4%e6%98%93%e6%89%8b%e6%95%b0%e6%98%af%e5%a4%9a%e5%b0%91%ef%bc%9f/`, template: 'knowledgebase2_2' },
    "/knowledge-base/spread/": { url: `${prefix}/knowledge-base/spread/`, template: 'knowledgebase2_3' },
    "/knowledge-base/trading-close/": { url: `${prefix}/knowledge-base/trading-close/`, template: 'knowledgebase2_4' },
    "/knowledge-base/%e5%a6%82%e4%bd%95%e5%81%9a%e9%a3%8e%e9%99%a9%e7%ae%a1%e7%90%86%ef%bc%9f/": { url: `${prefix}/knowledge-base/%e5%a6%82%e4%bd%95%e5%81%9a%e9%a3%8e%e9%99%a9%e7%ae%a1%e7%90%86%ef%bc%9f/`, template: 'knowledgebase3_1' },
    "/knowledge-base/trading-strategy/": { url: `${prefix}/knowledge-base/trading-strategy/`, template: 'knowledgebase3_2' },
    "/knowledge-base/howdoiknowmyaccountisactive/": { url: `${prefix}/knowledge-base/howdoiknowmyaccountisactive/`, template: 'knowledgebase4_1' },
    "/knowledge-base/realaccountvsdemoaccount/": { url: `${prefix}/knowledge-base/realaccountvsdemoaccount/`, template: 'knowledgebase4_2' },
    "/knowledge-base/what-is-forex/": { url: `${prefix}/knowledge-base/what-is-forex/`, template: 'knowledgebase5_1' },
    "/knowledge-base/advantage/": { url: `${prefix}/knowledge-base/advantage/`, template: 'knowledgebase5_2' },
    "/knowledge-base/howforexworks/": { url: `${prefix}/knowledge-base/howforexworks/`, template: 'knowledgebase5_3' },
    "/knowledge-base/forex-trading-risk-control/": { url: `${prefix}/knowledge-base/forex-trading-risk-control/`, template: 'knowledgebase5_4' },
    "/contactus/": { url: `${prefix}/contactus/`, template: 'contact_us' },
    "/spot-metals-commodities/": { url: `${prefix}/spot-metals-commodities/`, template: 'spot_metals_commodities' },
    "/spot-metals-commodities/spreads-and-specifications/": { url: `${prefix}/spot-metals-commodities/spreads-and-specifications/`, template: 'spreads_and_specifications' },
    "/spot-metals-commodities/trade-indices/": { url: `${prefix}/spot-metals-commodities/trade-indices/`, template: 'trade_indices' },
    "/spot-metals-commodities/forex/": { url: `${prefix}/spot-metals-commodities/forex/`, template: 'forex' },
    "/trading-platform/": { url: `${prefix}/trading-platform/`, template: 'trading_platform' },
    "/economic-calendar/": { url: `${prefix}/economic-calendar/`, template: 'economic_calendar' },
    "/economic-calendar/market-reports/": { url: `${prefix}/economic-calendar/market-reports/`, template: 'market_reports' },
    "/economic-calendar/technical-reports/": { url: `${prefix}/economic-calendar/technical-reports/`, template: 'technical_reports' },
    "/technical-charts/": { url: `${prefix}/technical-charts/`, template: 'technical_charts' },
  };
}
// 格式 url: { url: 'url', temaplate: views/*.pug }

const routersGenerator = (locale='en') => {
  const express = require('express');
  const router = express.Router();
  const path = require('path');
  const localeObject = require(path.resolve('locale', locale));
  const link = locale === 'en' ? linkGenerator() : linkGenerator(`/${locale}`);
  const linkArr = Object.keys(link);

  for(let i=0, max=linkArr.length; i < max; i++) {    
    router.get(linkArr[i], function(req, res, next) {
      res.render(link[linkArr[i]].template, {
        ...localeObject,
        productionMode: false,
        link,
      })
    })
  }

  return router;
}

module.exports = {
  linkGenerator,
  routersGenerator,
}