const pug = require('pug');
const sass = require('node-sass');
const path = require('path');
const fs = require('fs');
const { linkGenerator } = require(path.join(__dirname, 'routers.js'));
const isWin = process.platform === "win32";

const cssCompiler = () => {
  sass.render({
    file: path.resolve('src', 'scss', 'index.scss'),
    sourceMap: true,
  }, (error, result) => {
    if(error) console.error(error);
    let styleDir = path.resolve('public', 'stylesheets')
    if (!fs.existsSync(styleDir)){
      fs.mkdirSync(styleDir);
    }
    fs.writeFile(path.resolve('public', 'stylesheets', 'style.css'), result.css, e => { if(e) console.log(e) });
  });
}

const htmlCompiler = () => {
  const languageList = ['', 'en', 'tw', 'zh'];
  const htmlDir = path.join(__dirname, 'public', 'htmls');

  languageList.forEach((langItem) => {
    if (!fs.existsSync(path.join(htmlDir))) fs.mkdirSync(path.join(htmlDir));
    if (!fs.existsSync(path.join(htmlDir, langItem))) fs.mkdirSync(path.join(htmlDir, langItem));
    const link = (langItem === 'en' || langItem === '') ? linkGenerator() : linkGenerator(`/${langItem}`);
    Object.keys(link).forEach((linkItem) => {
      const locale = langItem == '' ? require(path.resolve('locale', 'en')) : require(path.resolve('locale', langItem));
      let htmlContent = pug.renderFile(path.resolve('views', `${link[linkItem].template}.pug`), { ...locale, link, productionMode: true });
      let fileNameArr = linkItem.split('/').filter(i => i);
      let targetDirPath = path.join.apply({}, [__dirname, 'public', langItem].concat(fileNameArr));
      createFolderRecursive(targetDirPath);
      fs.writeFile(path.join(targetDirPath, `index.html`), htmlContent, e => { if (e) console.log(e) });
    })
  })
}

const createFolderRecursive = (targetPath) => {
  if (fs.existsSync(targetPath)) return;
  let parentDirArray = targetPath.split(path.sep).filter(i => i);
  parentDirArray.splice(-1, 1);
  let parentDir = isWin ?
    path.join.apply({}, parentDirArray)
    :
    path.join.apply({}, [path.sep].concat(parentDirArray));

  if (fs.existsSync(parentDir)) {
    fs.mkdirSync(targetPath);
  } else {
    createFolderRecursive(parentDir);
    createFolderRecursive(targetPath);
  }
}

const deleteFolderRecursive = function(targetPath) {
  if (fs.existsSync(targetPath)) {
    fs.readdirSync(targetPath).forEach(function(file){
      var curPath = path.join(targetPath, file);
      if (fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(targetPath);
  }
};

const walkSync = dir => {
  return fs.readdirSync(dir).reduce((files, file) => {
    const name = path.join(dir, file);
    const fstate = fs.statSync(name);
    const isDirectory = fstate.isDirectory();
    let targetPathArr = name.split(path.sep).filter(i => i && i !== 'src');
    let targetIndex = targetPathArr.findIndex(i => i == 'static');
    targetPathArr.splice(targetIndex, 1, 'public');
    const currentDirFile = isWin ?
      path.join.apply({}, targetPathArr)
      :
      path.join.apply({}, [path.sep].concat(targetPathArr));
    if (isDirectory && !fs.existsSync(currentDirFile)) fs.mkdirSync(currentDirFile);
    if (!isDirectory && /\.(png|gif|jpe?g|bmp|ico|js|css)$/.test(name) && !/\.DS_Store$/.test(name)) {
      if(!fs.existsSync(currentDirFile)) {
        fs.copyFileSync(
          path.join(name),
          path.join(currentDirFile)
        );
      }
    }
    return isDirectory ? [...files, ...walkSync(name)] : files;
  }, []);
}

module.exports = {
  cssCompiler,
  htmlCompiler,
  deleteFolderRecursive,
  createFolderRecursive,
  walkSync,
};
