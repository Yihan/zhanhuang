# ZHAN HUANG

## Env
node v8.9.4^

## Devleopment
```
$ npm run dev
```

## Rebuild
```
$ npm run build
```

## Deployment
```
$ npm start
```

## Start with PM2
```
$ pm2 start ecosystem.config.js --env production

```

## 專案結構說明
```
rootDir
src (source 部署用的一切來源 js, scss, images)
dev.js (顧名思義 就是開發用打包入口 會將 index.js & scss資料夾下的檔案打包成虛擬檔案暫存於記憶體)