const path = require('path');
const fs = require('fs');
const { 
  cssCompiler,
  htmlCompiler,
  walkSync,
  deleteFolderRecursive
} = require(path.join(__dirname, 'compiler.js'));


if (fs.existsSync(path.join(__dirname, 'public'))) deleteFolderRecursive(path.join(__dirname, 'public'));
if (!fs.existsSync(path.join(__dirname, 'public'))) fs.mkdirSync(path.join(__dirname, 'public'));
walkSync(path.join(__dirname, 'src', 'static'));
cssCompiler();
htmlCompiler();