const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const favicon = require('serve-favicon');
const logger = require('morgan');
const { routersGenerator } = require(path.join(__dirname, 'routers.js'));
const app = express();
//
if (process.env.NODE_ENV == 'dev') {
  const fs = require('fs');
  const { deleteFolderRecursive } = require(path.join(__dirname, 'compiler.js'));
  if (fs.existsSync(path.join(__dirname, 'public'))) deleteFolderRecursive(path.join(__dirname, 'public'));
  const webpack = require('webpack');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');
  const config = require(path.join(__dirname, 'dev.config.js'));
  const compiler = webpack(config);

  app.use(webpackDevMiddleware(compiler, {
    hot: true,
    filename: 'bundle.js',
    publicPath: config.output.publicPath,
    stats: {
      colors: true,
    },
    historyApiFallback: true,
  }));

  app.use(webpackHotMiddleware(compiler, {
    log: console.log
  }));
} else {
  const fs = require('fs');
  const axios = require('axios');
  const { parseString } = require('xml2js');
  const feedParseUrl = 'http://cn.reuters.com/rssFeed/chinaNews/';
  const fileTargetPath = process.env.NODE_ENV == 'dev' ? 
    path.resolve('src', 'static', 'reuterslink.json')
    :
    path.resolve('public', 'reuterslink.json');
  const timer = async () => {
    try {
      axios.get(feedParseUrl)
      .then(({ data }) => {
        parseString(data, { trim: false }, function (err, { rss }) {
          if (err) console.log(err)
          if (!Array.isArray(rss.channel[0].item)) return;
          let itemArray = rss.channel[0].item;
          let newItemArray = itemArray.map((item) => {
            return {
              title: item.title[0],
              link: item.link[0],
              pubDate: item.pubDate[0],
            }
          })

          fs.writeFile(
            fileTargetPath,
            JSON.stringify({ entries: newItemArray }),
            (err) => {
              if (err) throw err;
            }
          );
        });
      })
    } catch (e) {
      console.log(e)
    }
  }
  timer();
  setInterval(timer, 60*60 *1000);
}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

if (process.env.NODE_ENV == 'dev') {
  app.use(favicon(path.join(__dirname, 'src', 'static', 'favicon.ico')));
  app.use(express.static(path.join(__dirname, 'src', 'static')));
  app.use(logger('dev'));
  app.use('/', routersGenerator('en'));
  app.use('/tw', routersGenerator('tw'));
  app.use('/zh', routersGenerator('zh'));
} else {
  app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
  app.use(express.static(path.join(__dirname, 'public')));
}

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.error(req)
  console.error(err)
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
